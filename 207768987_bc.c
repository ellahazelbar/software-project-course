#include <stdio.h>

int ProcessDigit(char c, int SBase)
{
	int dig = c - '0';
	if (dig < SBase) return dig;
	else return -1;
}

int ProcessChar(char c, int SBase)
{
	int dig = c - 'W'; /* 'W' = 'a' - 10 */ 
	if (dig < SBase) return dig;
	else return -1;
}

void PrintDigit(int Digit)
{
	if (Digit < 10) putchar('0' + Digit);
	else putchar('W' + Digit); /* 'W' = 'a' - 10 */ 
}

int PrintNthDigit(int Number, int TBase, int DigitIndex)
{
	int i;
	for (i = 0; i < DigitIndex; i++)
	{
		Number /= TBase;
	}
	PrintDigit(Number % TBase);
	for (i = 0; i < DigitIndex; i++)
	{
		Number *= TBase;
	}
	return Number;
}

int main()
{
	int sbase = -1, tbase = -1, result = 0, digits, convs, temp;
	char c;
	printf("enter the source base:\n");
	convs = scanf("%d", &sbase);
	if (0 == convs || sbase < 2 || 16 < sbase)
	{
		printf("Invalid source base!");
		return 1;
	}
	printf("enter the target base:\n");
	convs = scanf("%d", &tbase);
	if (0 == convs || tbase < 2 || 16 < tbase)
	{
		printf("Invalid target base!");
		return 1;
	}
	printf("enter a number in base %d:", sbase);
	c = fgetc(stdin);
	printf("%c", c);
	while ((c = getchar()) != '\0' && c != '\n' && c != '\r' && c != EOF)
	{
		int digit = -1;
		result *= sbase;
		if ('0' <= c && c <= '9')
		{
			digit = ProcessDigit(c, sbase);
		}
		else if ('a' <= c && c <= 'z')
		{ 
			digit = ProcessChar(c, sbase);
		}
		if (-1 == digit)
		{
			printf("Invalid input number!");
			return 1;
		}
		result += digit;
	}
	printf("the number in base %d is:\n", tbase);
	/*0-indexed digit counter, so "123" has 2 digits (a second, a first, and a zeroth)*/
	digits = 0; 
	temp = result;
	while (tbase <= temp)
	{
		temp /= tbase;
		++digits;
	}
	while (-1 < digits)
	{
		result -= PrintNthDigit(result, tbase, digits);
		--digits;
	}
	putchar('\r');
	putchar('\n');
	return 0;
}